#include <stdio.h>

#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>

#define NUM_TAREAS 4 //Inicia dese 1
 
// Ftiempo de espera   = te
 
void funcTiempoEspera(int procesos[], int n,   
                          int tp[], int te[])  
{  
    // el tiempo de espera empieza en 0 para el primer proceso  
    te[0] = 0;  
    
    // calculando PARA C SE NECESITA DECLARAR ANTES LA VARIABLE Y NO COLOCARLA COMO LOOP
    	int i;
        for ( i = 1; i < n ; i++ )  
        te[i] =  tp[i-1] + te[i-1] ;  
}  
    
// FLA DE RESPUESTA  = tr
void funcTiempoRespuesta( int procesos[], int n, int bt[], int te[], int tr[])  
{  
    // SUMA PARA LA RESPUESTA
    // bt[i] + wt[i] 
	int i; 
    for ( i = 0; i < n ; i++)  
        tr[i] = bt[i] + te[i];  
}  
    
//TIEMPO PROMEDIO  = tp
void funcTiempoPromedio( int procesos[], int n, int tp[])  
{  
    int te[n], tr[n], total_te = 0, total_tr = 0;  
    
    

    funcTiempoEspera(procesos, n, tp, te);  
    
   
    funcTiempoRespuesta(procesos, n, tp, te, tr);  
    
    //tareas



    
      
    printf("Proceso   Tiempo de inicio   Tiempo de Espera   Tiempo de Respuesta\n");  
    
    // Calculando el tiempo de espera y de respuesta  
    int i;
    for ( i=0; i<n; i++)  
    {  
        total_te = total_te + te[i];  
        total_tr = total_tr + tr[i]; 
        printf("   %d ",(i+1)); 
        printf("       %d\t\t ", tp[i] ); 
        printf("       %d\t\t",te[i] ); 
        printf("       %d\n",tr[i] );
 
       
    }  
    	//for (int  i=0; i++) {
    	//int i;
    	//i= 0;
    	//i= i++;
    	
    	//int i++;
    	//total_wt = total_wt + wt[i];  
        //total_tat = total_tat + tat[i]; 
        printf("\nLa tarea [%d], esta Escuchando Musica con un tiempo de respuesta de [%d]\n",/*(i+1),tat[i])*/(1), 10);
        printf("La tarea [%d], esta Leyendo un PDF con un tiempo de respuesta de [%d]\n", /*(i+1),tat[i])*/(2), 15);
        printf("La tarea [%d], esta Descargando un Archivo con un tiempo de respuesta de [%d]\n",/*(i+1),tat[i])*/ (3), 23);
        
		//}	
		//for(int j = 0; j < _array[i].delay; j++){
		    //process id's  
 
 
    
    int s=(float)total_te / (float)n; 
    int t=(float)total_tr / (float)n; 
    printf("\nTiempo de espera promedio = %d",s); 
    printf("\n"); 
    printf("Tiempo de respuesta promedio = %d\n\n ",t);  
    
    
    printf(":::EJECUCION DE LAS TAREAS A TRAVES DE POSIX THREADS:::\n\n ",t);  
}  
      
      
	/*
	struct tarea{
	int processes;
	int tat;
	char nombre;
	char procesoarealizar;
	
};
	tarea *creartarea(int processes, int tat, char *nombre, char *procesoarealizar);
    	tarea *t1 = creartarea(1, tat, "Compilar Programa");
    	tarea *t2 = creartarea(2, tat, "Jugar videojuegos");
    	tarea *t3 = creartarea(3, tat, "Leer un PDF");
    	
    for (int  i=0; i<n; i++)  
    {  
        total_wt = total_wt + wt[i];  
        total_tat = total_tat + tat[i]; 
        printf("   %d ",(i+1)); 
        printf("       %d\t\t ", bt[i] ); 
        printf("       %d\t",wt[i] ); 
        printf("       %d\n",tat[i] );  
        printf("La tarea ")
    }  */
	
    



  void *hacer_trabajo(void *argumentos){
  int index = *((int *)argumentos);
  int sleep_time = 3 + rand() % NUM_TAREAS;
  printf("Tarea %d: Inicio.\n", index);
  printf("Tarea %d: Suspendido por %d segundos.\n", index, sleep_time);
  sleep(sleep_time);
  printf("Tarea %d: Finalizado.\n", index);
  
}


int main(void)  
{  
    //Numero de procesos y su identificador 
    int procesos[] = {1, 2, 3};  
    int n = sizeof procesos / sizeof procesos[0];  
    

    //Tiempo de inicio de procesos  
    int  tiempoinicio[] = {10, 5, 8};  
    
    funcTiempoPromedio(procesos, n, tiempoinicio);  
    
	
	

  pthread_t tareas[NUM_TAREAS];
  int tareas_args[NUM_TAREAS];
  int j;
  int result_code;
  
  //Creando hilo por hilo, solo creacion
  for (j = 1; j < NUM_TAREAS; j++) {
    printf("\nEstado principal: Creando Tarea %d.\n", j);
    tareas_args[j] = j;
    result_code = pthread_create(&tareas[j], NULL, hacer_trabajo, &tareas_args[j]);
    assert(!result_code);
  }

  //printf("Estado principal: Hilos de las tareas creados.\n");

  //Espera del termino de hilo
  for (j = 1; j < NUM_TAREAS; j++) {
    result_code = pthread_join(tareas[j], NULL);
    assert(!result_code);
    printf("Estado principal: Tarea %d ha finalizado.\n", j);
  }

  printf("\nTodos las tareas han finalizado.\n");
  return 0;
		  
}  

